<?php

/**
 * @file
 * This module provide a field only for chinese identity card.
 */

define('CHINESE_IDENTITY_CARD_TYPE_ALL', 0);
define('CHINESE_IDENTITY_CARD_TYPE_15', 1);
define('CHINESE_IDENTITY_CARD_TYPE_18', 2);


/**
 * Implements hook_help().
 */
function chinese_identity_card_help($path, $arg) {
  $output = '';

  switch ($path) {
    case 'admin/help#chinese_identity_card':
      $output .= '<p>The Chinese Identity Card module is a small module. But it\'s useful for entering ID Card in china. No need to install field validation module or using drupal
form alter api to add validation handler. This module using text form element,
And adding module defined widget and validation.</p>';
      $output .= '<p>For more information, Please read README file under module path.</p>';

      break;

  }

  return $output;
}

/**
 * Implements hook_field_info().
 */
function chinese_identity_card_field_info() {
  return array(
    'chinese_identity_card' => array(
      'label' => t('Chinese identity card'),
      'description' => t('Chinese identity card'),
      'settings' => array(
        'chinese_identity_card_type' => CHINESE_IDENTITY_CARD_TYPE_ALL,
      ),
      'default_widget' => 'chinese_identity_card_default_widget',
      'default_formatter' => 'chinese_identity_card_default_formatter',
    ),
  );
}

/**
 * Implements hook_field_validate().
 */
function chinese_identity_card_field_validate($entity_type, $entity, $field, $instance, $langcode, $items, &$errors) {
  foreach ($items as $delta => $item) {
    if (!empty($item['chinese_identity_card'])) {
      $hook = 'chinese_identity_card_validate';
      $implements = module_implements($hook);
      if (count($implements)) {
        $result = true;
        foreach($implements as $module) {
          $function = $module . '_' . $hook;
          if(!$function($item['chinese_identity_card'])) {
            $result = false;
            break;
          }
        }

        if(!$result) {
          $errors[$field['field_name']][$langcode][$delta][] = array(
            'error' => 'chinese_identity_card_type',
            'message' => t('%name: the value may not be valid.', array(
              '%name' => $instance['label'],
            )),
          );
        }
      }
      else {
        switch ($field['settings']['chinese_identity_card_type']) {
          case CHINESE_IDENTITY_CARD_TYPE_15:
            if (!chinese_identity_card_type_check($item['chinese_identity_card'], CHINESE_IDENTITY_CARD_TYPE_15)) {
              $errors[$field['field_name']][$langcode][$delta][] = array(
                'error' => 'chinese_identity_card_type',
                'message' => t('%name: the value may not be the type of %type.', array(
                  '%name' => $instance['label'],
                  '%type' => t('15 Bits'),
                )),
              );
            }
            if (!chinese_identity_card_check($item['chinese_identity_card'])) {
              $errors[$field['field_name']][$langcode][$delta][] = array(
                'error' => 'chinese_identity_card_check',
                'message' => t('%name: the value may not be valid', array(
                  '%name' => $instance['label'],
                )),
              );
            }
            break;

          case CHINESE_IDENTITY_CARD_TYPE_18:
            if (!chinese_identity_card_type_check($item['chinese_identity_card'], CHINESE_IDENTITY_CARD_TYPE_18)) {
              $errors[$field['field_name']][$langcode][$delta][] = array(
                'error' => 'chinese_identity_card_type',
                'message' => t('%name: the value may not be the type of %type.', array(
                  '%name' => $instance['label'],
                  '%type' => t('18 Bits'),
                )),
              );
            }
            if (!chinese_identity_card_check($item['chinese_identity_card'])) {
              $errors[$field['field_name']][$langcode][$delta][] = array(
                'error' => 'chinese_identity_card_check',
                'message' => t('%name: the value may not be valid', array(
                  '%name' => $instance['label'],
                )),
              );
            }
            break;

          default:
            // For CHINESE_IDENTITY_CARD_TYPE_ALL:
            if (!chinese_identity_card_check($item['chinese_identity_card'])) {
              $errors[$field['field_name']][$langcode][$delta][] = array(
                'error' => 'chinese_identity_card_check',
                'message' => t('%name: the value may not be valid', array(
                  '%name' => $instance['label'],
                )),
              );
            }
        }
      }
    }
  }
}

/**
 * Implements hook_field_is_empty().
 */
function chinese_identity_card_field_is_empty($item, $field) {
  return empty($item['chinese_identity_card']);
}

/**
 * Implements hook_field_settings_form().
 */
function chinese_identity_card_field_settings_form($field, $instance, $has_data) {
  $settings = $field['settings'];

  $form['chinese_identity_card_type'] = array(
    '#type' => 'select',
    '#title' => t('Chinese identity card type'),
    '#options' => array(
      CHINESE_IDENTITY_CARD_TYPE_ALL => t('All'),
      CHINESE_IDENTITY_CARD_TYPE_15 => t('15 Bits'),
      CHINESE_IDENTITY_CARD_TYPE_18 => t('18 Bits'),
    ),
    '#disabled' => $has_data,
    '#default_value' => $settings['chinese_identity_card_type'],
    '#description' => t('Select which type of chinese identity card you would use.'),
  );

  return $form;
}

/**
 * Implements hook_field_formatter_info().
 */
function chinese_identity_card_field_formatter_info() {
  return array(
    'chinese_identity_card_default_formatter' => array(
      'label' => t('Chinese identity card default'),
      'field types' => array('chinese_identity_card'),
    ),
  );
}

/**
 * Implements hook_field_formatter_view().
 */
function chinese_identity_card_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();
  switch ($display['type']) {
    case 'chinese_identity_card_default_formatter':
      foreach ($items as $delta => $item) {
        $element[$delta] = array(
          '#theme' => 'html_tag__chinese_identity_card',
          '#tag' => 'p',
          '#value' => check_plain($item['chinese_identity_card']),
        );
      }
      break;
  }
  return $element;
}

/**
 * Implements hook_field_widget_info().
 */
function chinese_identity_card_field_widget_info() {
  return array(
    'chinese_identity_card_default_widget' => array(
      'label' => t('Chinese identity card'),
      'field types' => array('chinese_identity_card'),
    ),
  );
}

/**
 * Implements hook_field_widget_form().
 */
function chinese_identity_card_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  $value = isset($items[$delta]['chinese_identity_card']) ? $items[$delta]['chinese_identity_card'] : '';

  switch ($instance['widget']['type']) {
    case 'chinese_identity_card_default_widget':
      $element['chinese_identity_card'] = array(
        '#type' => 'textfield',
        '#title' => isset($instance['label']) ? $instance['label'] : t('Identity card'),
        '#default_value' => $value,
        '#required' => $instance['required']
      );
      break;
  }

  return $element;
}

/**
 * Checking the type of chinese identity card.
 *
 * @param string $chinese_identity_card
 *   Chinese identity card.
 * @param string $chinese_identity_card_type
 *   The type for chinese identity card.
 *
 * @return bool
 *   TRUE or FALSE
 */
function chinese_identity_card_type_check($chinese_identity_card, $chinese_identity_card_type) {
  switch ($chinese_identity_card_type) {
    case CHINESE_IDENTITY_CARD_TYPE_15:
      if (!preg_match('/^\d{15}$/i', $chinese_identity_card)) {
        return FALSE;
      }
      break;

    case CHINESE_IDENTITY_CARD_TYPE_18:
      if (!preg_match('/^\d{17}(\d|x)$/i', $chinese_identity_card)) {
        return FALSE;
      }
      break;

    default:
      // For CHINESE_IDENTITY_CARD_TYPE_ALL:
      if (!preg_match('/^\d{17}(\d|x)$/i', $chinese_identity_card) and !preg_match('/^\d{15}$/i', $chinese_identity_card)) {
        return FALSE;
      }
  }
  return TRUE;
}

/**
 * Checking chinese identity card.
 *
 * @param string $chinese_identity_card
 *   Chinese identity card.
 *
 * @return bool
 *   TRUE or FALSE.
 */
function chinese_identity_card_check($chinese_identity_card) {
  $city = array(
    11 => "北京",
    12 => "天津",
    13 => "河北",
    14 => "山西",
    15 => "内蒙古",
    21 => "辽宁",
    22 => "吉林",
    23 => "黑龙江",
    31 => "上海",
    32 => "江苏",
    33 => "浙江",
    34 => "安徽",
    35 => "福建",
    36 => "江西",
    37 => "山东",
    41 => "河南",
    42 => "湖北",
    43 => "湖南",
    44 => "广东",
    45 => "广西",
    46 => "海南",
    50 => "重庆",
    51 => "四川",
    52 => "贵州",
    53 => "云南",
    54 => "西藏",
    61 => "陕西",
    62 => "甘肃",
    63 => "青海",
    64 => "宁夏",
    65 => "新疆",
    71 => "台湾",
    81 => "香港",
    82 => "澳门",
    91 => "国外",
  );
  $id_card_length = strlen($chinese_identity_card);

  // Length checking.
  if (!preg_match('/^\d{17}(\d|x)$/i', $chinese_identity_card) and !preg_match('/^\d{15}$/i', $chinese_identity_card)) {
    return FALSE;
  }

  // Area checking.
  $city_code = array_keys($city);
  if (!in_array(intval(substr($chinese_identity_card, 0, 2)), $city_code)) {
    return FALSE;
  }
  // 15bits card checks the birthday. and convert 18bits.
  if ($id_card_length == 15) {
    $year = '19' . substr($chinese_identity_card, 6, 2);
    $month = substr($chinese_identity_card, 8, 2);
    $day = substr($chinese_identity_card, 10, 2);
    if (!checkdate($month, $day, $year)) {
      return FALSE;
    }
    $s_birthday = $year . '-' . $month . '-' . $day;

    $d = new DateTime($s_birthday);
    $dd = $d->format('Y-m-d');
    if ($s_birthday != $dd) {
      return FALSE;
    }
    // 15 to 18.
    $chinese_identity_card = substr($chinese_identity_card, 0, 6) . "19" . substr($chinese_identity_card, 6, 9);
    // Calculate the checksum of 18bits card.
    $bit_18 = chinese_identity_card_get_verify_bit($chinese_identity_card);
    $chinese_identity_card = $chinese_identity_card . $bit_18;
  }
  // Checking whether the year bigger than 2078, and less than 1900.
  $year = substr($chinese_identity_card, 6, 4);
  if ($year < 1900 || $year > 2078) {
    return FALSE;
  }

  // Handle 18bit card.
  $s_birthday = substr($chinese_identity_card, 6, 4) . '-' . substr($chinese_identity_card, 10, 2) . '-' . substr($chinese_identity_card, 12, 2);
  $d = new DateTime($s_birthday);
  $dd = $d->format('Y-m-d');
  if ($s_birthday != $dd) {
    return FALSE;
  }

  // Checking chinese identity card standard.
  $chinese_identity_card_base = substr($chinese_identity_card, 0, 17);
  if (strtoupper(substr($chinese_identity_card, 17, 1)) != chinese_identity_card_get_verify_bit($chinese_identity_card_base)) {
    return FALSE;
  }
  return TRUE;
}


/**
 * Calculate the checksum of chinese identity card.
 *
 * Using country standard:GB11643-1999.
 *
 * @param string $chinese_identity_card_base
 *   The base of Chinese identity card.
 *
 * @return bool
 *   TRUE or FALSE.
 */
function chinese_identity_card_get_verify_bit($chinese_identity_card_base) {
  if (strlen($chinese_identity_card_base) != 17) {
    return FALSE;
  }
  // Weighting factor.
  $factor = array(7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2);
  // Check code corresponding to the value.
  $verify_number_list = array(
    '1',
    '0',
    'X',
    '9',
    '8',
    '7',
    '6',
    '5',
    '4',
    '3',
    '2',
  );
  $checksum = 0;
  for ($i = 0; $i < strlen($chinese_identity_card_base); $i++) {
    $checksum += substr($chinese_identity_card_base, $i, 1) * $factor[$i];
  }
  $mod = $checksum % 11;
  $verify_number = $verify_number_list[$mod];
  return $verify_number;
}
